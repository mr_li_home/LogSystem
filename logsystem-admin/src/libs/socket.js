import Util from '@/libs/util';
// import {webSocketUrl} from '_conf/url';

const socketInstance = (webSocketUrl) => {
    let url = webSocketUrl;
    let token = Util.getToken();
    let address = `${url}?token=${token}`;
    let socket = new WebSocket(address);
    return socket;
};

export default socketInstance;
