import env from './env';

if (!window.configData) {
    window.configData = {};
}

const getDevUrl = () => {
    if (window.configData.devUrl)
        return window.configData.devUrl;
    else
        return '';
};

const getProUrl = () => {
    if (window.configData.proUrl)
        return window.configData.proUrl;
    else
        return '';
};

const getWebSocketUrl = () => {
    if (window.configData.webSocketUrl)
        return window.configData.webSocketUrl;
    else
        return '';
};

const getUrl = () => {
    return env === 'development' ? getDevUrl() : getProUrl();
};

const DEV_URL = '';
// const DEV_URL = 'http://118.31.35.208:1345/';
const PRO_URL = window.configData.proUrl;

//WebSocket地址
const WebSocketUrl = window.configData.webSocketUrl;

export default getUrl;

export const webSocketUrl = getWebSocketUrl();
