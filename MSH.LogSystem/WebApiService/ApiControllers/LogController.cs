﻿using BusinessLayer.Interface;
using Common;
using Configuration;
using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApiService.Core.ApiControllers
{
    /// <summary>
    /// 日志管理(综合)
    /// </summary>
    public class LogController : ManagerController
    {
        public IDebugLogManager IDebugLogManager { get; set; }
        public IWarnLogManager IWarnLogManager { get; set; }
        public IErrorLogManager IErrorLogManager { get; set; }
        public IInfoLogManager IInfoLogManager { get; set; }

        /// <summary>
        /// 综合查询日志
        /// </summary>
        /// <param name="query">查询条件</param>
        /// <returns></returns>
        [HttpGet]
        public AjaxReturnInfo Query([FromUri] LogQuery query)
        {
            query.Pagination = new Pagination() { IsPaging = false };
            var datas = new List<LogInfo>();

            //Info日志
            var infos = IInfoLogManager.QueryLogInfo(query);
            infos.ForEach(a =>
            {
                a.LogLevelName = LogLevel.Info.NoteName();
            });
            datas.AddRange(infos);

            //Warn日志
            var warns = IWarnLogManager.QueryLogInfo(query);
            warns.ForEach(a =>
            {
                a.LogLevelName = LogLevel.Warn.NoteName();
            });
            datas.AddRange(warns);

            //Debug日志
            var debugs = IDebugLogManager.QueryLogInfo(query);
            debugs.ForEach(a =>
            {
                a.LogLevelName = LogLevel.Debug.NoteName();
            });
            datas.AddRange(debugs);

            //Error日志
            var errors = IErrorLogManager.QueryLogInfo(query);
            errors.ForEach(a =>
            {
                a.LogLevelName = LogLevel.Error.NoteName();
            });
            datas.AddRange(errors);

            datas = datas.OrderByDescending(a => a.CreationTime).ToList();

            return new AjaxReturnInfo()
            {
                Data = new
                {
                    List = datas,
                }
            };
        }
    }
}
