﻿using System;
using System.Diagnostics;
using LiteDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LiteDbTest
{
    [TestClass]
    public class 测试插入
    {
        [TestMethod]
        public void 单条插入()
        {
            var watch = new Stopwatch();
            watch.Start();
            // Open database (or create if not exits)
            using (var db = new LiteDatabase("LogObject.db"))
            {
                // Get customer collection
                var collection = db.GetCollection<LogObject>();

                // Insert new customer document (Id will be auto-incremented)
                collection.Insert(new LogObject()
                {
                    
                    Name="Hello",
                    Age=4545
                });
            }
            watch.Stop();
            Console.WriteLine($"单条执行时间:{watch.ElapsedMilliseconds}毫秒");
        }

        [TestMethod]
        public void 批量插入1W()
        {
            var watch = new Stopwatch();
            watch.Start();
            // Open database (or create if not exits)
            using (var db = new LiteDatabase("LogObject.db"))
            {
                // Get customer collection
                var collection = db.GetCollection<LogObject>();

                for (int i = 0; i <= 10000; i++)
                {
                    collection.Insert(new LogObject()
                    {

                        Name = "Hello",
                        Age = 4545
                    });
                }
            }
            watch.Stop();
            Console.WriteLine($"1W执行时间:{watch.ElapsedMilliseconds}毫秒");
        }

        [TestMethod]
        public void 批量插入10W()
        {
            var watch = new Stopwatch();
            watch.Start();
            // Open database (or create if not exits)
            using (var db = new LiteDatabase("LogObject.db"))
            {
                // Get customer collection
                var collection = db.GetCollection<LogObject>();

                for (int i = 0; i <= 100000; i++)
                {
                    collection.Insert(new LogObject()
                    {

                        Name = "Hello",
                        Age = 4545
                    });
                }
            }
            watch.Stop();
            Console.WriteLine($"10W执行时间:{watch.ElapsedMilliseconds}毫秒");
        }
    }
}
